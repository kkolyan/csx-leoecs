using System;
using System.Collections;
using System.Collections.Generic;
using Leopotam.Ecs;

namespace Kk.CsxLeoEcs
{
    public readonly struct EcsCursorDriver<T> : IEnumerable<EcsCursor<T>>
        where T : struct
    {
        private static EcsCursor<T> _cursor = new EcsCursor<T>();

        // ReSharper disable once StaticMemberInGenericType
        private static bool _locked; //it's intended to be unique for each specialization

        private readonly EcsFilter<T> _filter;

        public EcsCursorDriver(EcsFilter<T> filter)
        {
            _filter = filter;
        }

        public int GetEntitiesCount() => _filter.GetEntitiesCount();

        public Enumerator GetEnumerator() => new Enumerator(_filter);
        IEnumerator<EcsCursor<T>> IEnumerable<EcsCursor<T>>.GetEnumerator() => GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public struct Enumerator : IEnumerator<EcsCursor<T>>
        {
            private readonly EcsFilter<T> _filter;
            private EcsFilter.Enumerator _enumerator;

            internal Enumerator(EcsFilter<T> filter) : this()
            {
                if (_locked)
                {
                    throw new Exception("only one iterator of the same generic type could be used at time.");
                }

                _locked = true;

                _filter = filter;
                _enumerator = _filter.GetEnumerator();
            }

            public void Reset() => _enumerator = _filter.GetEnumerator();
            object IEnumerator.Current => Current;
            public EcsCursor<T> Current => _cursor;

            public bool MoveNext()
            {
                if (!_enumerator.MoveNext()) return false;
                _cursor.filter = _filter;
                _cursor.index = _enumerator.Current;
                return true;
            }

            public void Dispose()
            {
                _enumerator.Dispose();
                _locked = false;
            }
        }
    }

    public readonly struct EcsCursorDriver<T1, T2> : IEnumerable<EcsCursor<T1, T2>>
        where T1 : struct
        where T2 : struct
    {
        private static EcsCursor<T1, T2> _cursor = new EcsCursor<T1, T2>();

        // ReSharper disable once StaticMemberInGenericType
        private static bool _locked; //it's intended to be unique for each specialization

        private readonly EcsFilter<T1, T2> _filter;

        public EcsCursorDriver(EcsFilter<T1, T2> filter)
        {
            _filter = filter;
        }

        public int GetEntitiesCount() => _filter.GetEntitiesCount();

        public Enumerator GetEnumerator() => new Enumerator(_filter);
        IEnumerator<EcsCursor<T1, T2>> IEnumerable<EcsCursor<T1, T2>>.GetEnumerator() => GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public struct Enumerator : IEnumerator<EcsCursor<T1, T2>>
        {
            private readonly EcsFilter<T1, T2> _filter;
            private EcsFilter.Enumerator _enumerator;

            public Enumerator(EcsFilter<T1, T2> filter) : this()
            {
                if (_locked)
                {
                    throw new Exception("only one iterator of the same generic type could be used at time.");
                }

                _locked = true;

                _filter = filter;
                _enumerator = _filter.GetEnumerator();
            }

            public void Reset() => _enumerator = _filter.GetEnumerator();
            object IEnumerator.Current => Current;
            public EcsCursor<T1, T2> Current => _cursor;

            public bool MoveNext()
            {
                if (!_enumerator.MoveNext()) return false;
                _cursor._filter = _filter;
                _cursor._index = _enumerator.Current;
                return true;
            }

            public void Dispose()
            {
                _enumerator.Dispose();
                _locked = false;
            }
        }
    }
}