using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Leopotam.Ecs;
using Object = UnityEngine.Object;

namespace Kk.CsxLeoEcs
{
    public static class EcsToStringExtensions
    {
        internal interface IAutoStringProxy
        {
            object GetAutoStringProxyTarget();
        }
        
        public static string AutoToString(this object o)
        {
            string result = AutoToString(o, new HashSet<object>(), 100, new HashSet<EcsEntity>());
            // throw new Exception(result.Replace("\"", "\\\""));
            return result;
        }

        private static string AutoToString(object o, HashSet<object> currentlyIn, int remainingDepth, HashSet<EcsEntity> ecsEntities)
        {
            if (o == null)
            {
                return "null";
            }

            Type type = o.GetType();
            if (type.IsPrimitive)
            {
                return o.ToString();
            }

            if (o is string || type.IsEnum)
            {
                return $@"""{o}""";
            }

            if (o is IAutoStringProxy)
            {
                IAutoStringProxy proxy = (IAutoStringProxy) o;
                return AutoToString(proxy.GetAutoStringProxyTarget(), currentlyIn, remainingDepth - 1, ecsEntities);
            }

            if (o is EcsEntity)
            {
                EcsEntity entity = (EcsEntity) o;
                if (!ecsEntities.Add(entity))
                {
                    return
                        $@"{{""@type"": ""Leopotam.Ecs.EcsEntity"", ""ID"": ""{entity.GetInternalId()}-{entity.GetInternalGen()}"", ""alive"": {entity.IsAlive()}, ""reference"": true}}";
                }

                object[] comps = Array.Empty<object>();
                if (!entity.IsNull())
                {
                    entity.GetComponentValues(ref comps);
                }

                string componentTypes = string.Join(", ", comps.Select(x => $@"""{x.GetType().FullName}""").ToArray());
                string componentsText = string.Join(", ",
                    comps.Select(x => $@"""{x.GetType().FullName}"": {AutoToString(x, currentlyIn, remainingDepth - 1, ecsEntities)}").ToArray());
                return
                    $@"{{""@type"": ""Leopotam.Ecs.EcsEntity"", ""ID"": ""{entity.GetInternalId()}-{entity.GetInternalGen()}"", ""alive"": {entity.IsAlive()}, ""header"": [{componentTypes}], ""data"": {{{componentsText}}}}}";
            }

            if (type.IsSubclassOf(typeof(Object)))
            {
                Object uo = (Object) o;
                if (uo == null)
                {
                    return $@"{{""@type"": ""{type.FullName}"", ""alive"": false, ""ID"": {uo.GetInstanceID()}, ""ToString"": ""{uo}""}}";
                }

                return $@"{{""@type"": ""{type.FullName}"", ""ID"": {uo.GetInstanceID()}, ""name"": ""{uo.name}""}}";
            }

            if (remainingDepth <= 0)
            {
                return $@"{{""@type"": ""{type}"", ""@error"": ""(TOO_DEEP)""}}";
            }

            if (!currentlyIn.Add(o))
            {
                // loops of simple managed objects are not supported (but entities loops are ok)
                return $@"{{""@type"": ""{type}"", ""@error"": ""(LOOP)""}}";
            }

            try
            {
                if (type.IsArray || type.GetMethod("GetEnumerator") != null)
                {
                    ReflectionEnumerable en = new ReflectionEnumerable(o);
                    string[] items = en.Select(o1 => AutoToString(o1, currentlyIn, remainingDepth - 1, ecsEntities)).ToArray();
                    return $@"{{""@type"": ""{type.FullName}"", ""items"": [{string.Join(", ", items)}]}}";
                }

                FieldInfo[] fields = type.GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);

                string[] components = fields
                    .Select(field => $@"""{field.Name}"": {AutoToString(field.GetValue(o), currentlyIn, remainingDepth - 1, ecsEntities)}").ToArray();
                if (type.IsValueType)
                {
                    return $@"{{{string.Join(", ", components)}}}";
                }

                return $@"{{""@type"": ""{type.FullName}"", ""fields"": {{{string.Join(", ", components)}}}}}";
            }
            finally
            {
                currentlyIn.Remove(o);
            }
        }
    }
}