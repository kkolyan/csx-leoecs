using System;
using System.Collections.Generic;
using Kk.CsxUnity;
using Leopotam.Ecs;
using UnityEngine;
using LeoSystems = Leopotam.Ecs.EcsSystems;

namespace Kk.CsxLeoEcs.UnityX
{
    public class EcsWorldUnityContainer : MonoBehaviour
    {
        private LeoSystems _systems;
        // serialization/deserialization support planned
        private EcsWorld _ecsWorld;
        
        [InspectorReadOnly][SerializeField] private bool initialized;

        public void Init(IEnumerable<IEcsSystem> systems, IEnumerable<object> injectables)
        {
            _ecsWorld = new EcsWorld();

            _systems = new LeoSystems(_ecsWorld);

            foreach (var system in systems)
            {
                _systems.Add(system);
            }
            
            foreach (var injectable in injectables)
            {
                _systems.Inject(injectable);
            }

            _systems.Init();

            initialized = true;
        }

        private void Update()
        {
            if (!initialized)
            {
                throw new Exception("Init should be invoked before");
            }
            if (Time.timeScale > Mathf.Epsilon)
            {
                _systems?.Run();
            }
        }

        private void OnDestroy()
        {
            _systems?.Destroy();
            _ecsWorld?.Destroy();
            initialized = false;
        }
    }
}