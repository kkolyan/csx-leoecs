using System.Collections.Generic;
using Kk.CsxLeoEcs.DependencyCheck;
using UnityEngine;

namespace Kk.CsxLeoEcs.UnityX
{
    [RequireComponent(typeof(EcsWorldUnityContainer))]
    public abstract class ManualOrderEcsInitializer : MonoBehaviour
    {
        public List<Object> unityInjectables;

        protected abstract void Configure(ISystemListBuilder builder, IList<object> injectables);

        private void OnEnable()
        {
            var ecs = GetComponent<EcsWorldUnityContainer>();
            var builder = new SystemListBuilder();

            var allInjectables = new List<object>();
            allInjectables.AddRange(unityInjectables);

            Configure(builder, allInjectables);

            ecs.Init(
                systems: builder.ToList(),
                injectables: allInjectables
            );
        }
    }
}