using System.Collections.Generic;
using System.IO;
using Kk.CsxLeoEcs.DependencyAutoSort;
using UnityEngine;

namespace Kk.CsxLeoEcs.UnityX
{
    [RequireComponent(typeof(EcsWorldUnityContainer))]
    public abstract class EcsInitializer<T> : MonoBehaviour
    {
        public List<Object> unityInjectables;

        protected abstract void Configure(AutoSortedSystemListBuilder<T> builder, IList<object> injectables);

        private void OnEnable()
        {
            var ecs = GetComponent<EcsWorldUnityContainer>();
            var builder = new AutoSortedSystemListBuilder<T>();
            
            var allInjectables = new List<object>();
            allInjectables.AddRange(unityInjectables);

            Configure(builder, allInjectables);

#if UNITY_EDITOR
            var report = string.Join(
                "\n",
                builder.GetOrderReport()
            );
            File.WriteAllText("EcsInitializer.SystemsOrderLock.txt", report);
#endif
            ecs.Init(
                systems: builder.BuildSortedSystemList(),
                injectables: allInjectables
            );
        }
    }
}