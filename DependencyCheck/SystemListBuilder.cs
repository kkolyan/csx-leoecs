using System.Collections.Generic;
using Leopotam.Ecs;

namespace Kk.CsxLeoEcs.DependencyCheck
{
    public class SystemListBuilder: ISystemListBuilder
    {
        private readonly List<IEcsSystem> _systems = new List<IEcsSystem>();
        
        public EcsSystemRegistration Add<T>()
            where T : IEcsSystem, new()
        {
            _systems.Add(new T());
            return new EcsSystemRegistration();
        }

        public List<IEcsSystem> ToList()
        {
            return _systems;
        }
    }
}