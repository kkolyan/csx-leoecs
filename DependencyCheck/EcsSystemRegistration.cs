namespace Kk.CsxLeoEcs.DependencyCheck
{
    public class EcsSystemRegistration
    {
        public EcsSystemRegistration DependsOn(EcsSystemRegistration another)
        {
            return this;
        }

        public EcsSystemRegistration Consumes<T>(in Barrier<T> barrier) where T : struct
        {
            return this;
        }

        public EcsSystemRegistration Produces<T>(in Barrier<T> barrier) where T : struct
        {
            return this;
        }

        public EcsSystemRegistration Consumes<T>() where T : struct
        {
            return this;
        }

        public EcsSystemRegistration Produces<T>() where T : struct
        {
            return this;
        }

        public EcsSystemRegistration Before(object phase)
        {
            return this;
        }

        public EcsSystemRegistration After(object phase)
        {
            return this;
        }
    }
}