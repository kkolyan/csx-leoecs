using Leopotam.Ecs;

namespace Kk.CsxLeoEcs.DependencyCheck
{
    public interface ISystemListBuilder
    {
        EcsSystemRegistration Add<TSystem>() where TSystem : IEcsSystem, new();
    }
}