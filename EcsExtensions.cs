using System;
using System.Runtime.CompilerServices;
using Leopotam.Ecs;

namespace Kk.CsxLeoEcs
{
    public static class EcsExtensions
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static EcsEntityId GetEntityId(this EcsEntity entity)
        {
            return new EcsEntityId(entity.GetInternalId(), entity.GetInternalGen());
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static EcsFilter<T> Query<T>(this EcsWorld world) where T : struct
        {
            return (EcsFilter<T>) world.GetFilter(typeof(EcsFilter<T>));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static EcsFilter<T1, T2> Query<T1, T2>(this EcsWorld world)
            where T1 : struct
            where T2 : struct
        {
            return (EcsFilter<T1, T2>) world.GetFilter(typeof(EcsFilter<T1, T2>));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static EcsFilter<T1, T2, T3> Query<T1, T2, T3>(this EcsWorld world)
            where T1 : struct
            where T2 : struct
            where T3 : struct
        {
            return (EcsFilter<T1, T2, T3>) world.GetFilter(typeof(EcsFilter<T1, T2, T3>));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T SingleOrEmpty<T>(this T filter)
            where T : EcsFilter
        {
            if (filter.GetEntitiesCount() > 1)
            {
                throw new Exception(
                    $"optional singleton expected, but {filter.GetEntitiesCount()} entities matched by {filter}"
                );
            }

            return filter;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T? GetIfHas<T>(this EcsEntity entity)
            where T : struct
        {
            if (entity.Has<T>())
            {
                return entity.Get<T>();
            }

            return null;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T? GetOrNull<T>(in this EcsEntity entity)
            where T : struct
        {
            if (!entity.IsAlive())
            {
                return null;
            }

            if (!entity.Has<T>())
            {
                return null;
            }

            return entity.Get<T>();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static ref T Req<T>(in this EcsEntity entity)
            where T : struct
        {
            if (!entity.IsAlive())
            {
                throw new Exception($"cannot access destroyed entity: {entity}");
            }

            if (!entity.Has<T>())
            {
                throw new Exception($"component not found: {typeof(T)} on entity {entity.GetEntityId()}. entity: {entity.AutoToString()}");
            }

            return ref entity.Get<T>();
        }

        internal static EcsFilter<T> GetFilter<T>(this EcsWorld world) where T : struct
        {
            return (EcsFilter<T>) world.GetFilter(typeof(EcsFilter<T>));
        }

        internal static EcsFilter<T1, T2> GetFilter<T1, T2>(this EcsWorld world)
            where T2 : struct
            where T1 : struct
        {
            return (EcsFilter<T1, T2>) world.GetFilter(typeof(EcsFilter<T1, T2>));
        }

        // ReSharper disable once UnusedTypeParameter
        private static class FirstFilterIndexHolder<T>
        {
            // ReSharper disable once StaticMemberInGenericType
            internal static int firstFilterIndex;
        }

        public static EcsEntity SingleEntityOrDefault<T>(this EcsWorld world) where T : struct
        {
            EcsFilter<T> filter = world.GetFilter<T>();
            using (EcsFilter.Enumerator en = filter.GetEnumerator())
            {
                if (en.MoveNext())
                {
                    if (filter.GetEntitiesCount() == 1)
                    {
                        ref int index = ref FirstFilterIndexHolder<T>.firstFilterIndex;
                        index = en.Current;
                        return filter.GetEntity(index);
                    }
                    throw new Exception(
                        $"singleton expected, but {filter.GetEntitiesCount()} entities matched: {filter.Cursor().AutoToString()}"
                    );
                }

                return default;
            }
        }

        public static ref T Single<T>(this EcsWorld world) where T : struct
        {
            EcsFilter<T> filter = world.GetFilter<T>();
            using (EcsFilter.Enumerator en = filter.GetEnumerator())
            {
                if (en.MoveNext())
                {
                    if (filter.GetEntitiesCount() == 1)
                    {
                        ref int index = ref FirstFilterIndexHolder<T>.firstFilterIndex;
                        index = en.Current;
                        return ref filter.Get1(index);
                    }
                }

                throw new Exception(
                    $"singleton expected, but {filter.GetEntitiesCount()} entities matched: {filter.Cursor().AutoToString()}"
                );
            }
        }

        public static T? SingleOrNull<T>(this EcsWorld world) where T : struct
        {
            EcsFilter<T> filter = world.GetFilter<T>();
            using (EcsFilter.Enumerator en = filter.GetEnumerator())
            {
                if (en.MoveNext())
                {
                    if (filter.GetEntitiesCount() == 1)
                    {
                        ref int index = ref FirstFilterIndexHolder<T>.firstFilterIndex;
                        index = en.Current;
                        return filter.Get1(index);
                    }

                    throw new Exception(
                        $"singleton expected, but {filter.GetEntitiesCount()} entities matched: {filter.Cursor().AutoToString()}"
                    );
                }

                return null;
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static EcsComponentRef<T> SingleRef<T>(this EcsFilter<T> filter) where T : struct
        {
            if (filter.GetEntitiesCount() > 1)
            {
                throw new Exception(
                    $"singleton expected, but {filter.GetEntitiesCount()} entities matched by {filter}"
                );
            }

            foreach (int idx in filter)
            {
                return filter.Get1Ref(idx);
            }

            throw new Exception($"singleton expected, but nothing matched by {filter}");
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static EcsComponentRef<T>? SingleRefOrNull<T>(this EcsFilter<T> filter) where T : struct
        {
            if (filter.GetEntitiesCount() > 1)
            {
                throw new Exception(
                    $"optional singleton expected, but {filter.GetEntitiesCount()} entities matched by {filter}"
                );
            }

            foreach (int idx in filter)
            {
                return filter.Get1Ref(idx);
            }

            return null;
        }
        
        public static EcsEntity Alive(this in EcsEntity entity)
        {
            if (entity.IsNull())
            {
                throw new Exception("entity is null");
            }

            if (!entity.IsAlive())
            {
                throw new Exception($"$entity is not alive: {entity}");
            }

            return entity;
        }

        public static void Add(this in EcsEntity entity, in EcsEntity componentValue)
        {
            throw new Exception("looks like mistake. EcsEntity is not intended to be a component type, though it technically can be.");
        }

        public static EcsEntity Add<T>(this in EcsEntity entity, in T componentValue) where T : struct
        {
            if (entity.Has<T>())
            {
                throw new Exception($"component {typeof(T).FullName} is already exist. entity: {entity}, componentValue: {componentValue}. entity: {entity.AutoToString()}");
            }

            entity.Replace(componentValue);
            return entity;
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int DestroyAll(this EcsFilter filter)
        {
            int destroyed = 0;
            foreach (int i in filter)
            {
                filter.GetEntity(i).DestroyLog();
                destroyed++;
            }

            return destroyed;
        }
    }
}