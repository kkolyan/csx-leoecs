using System;
using System.Runtime.CompilerServices;
using Leopotam.Ecs;

namespace Kk.CsxLeoEcs
{
    public static class EcsCursorExtensions
    {
        public static EcsCursorDriver<T> Cursor<T>(this EcsFilter<T> filter)
            where T : struct
        {
            return new EcsCursorDriver<T>(filter);
        }

        public static EcsCursorDriver<T1, T2> Cursor<T1, T2>(this EcsFilter<T1, T2> filter)
            where T1 : struct
            where T2 : struct
        {
            return new EcsCursorDriver<T1, T2>(filter);
        }

        private static bool IsCursorAlive<T>(EcsCursor<T> x) where T : struct
        {
            return x.GetEntity().IsAlive();
        }

        public static EcsCursorDriver<T> Cursor<T>(this EcsWorld world) where T : struct
        {
            return world.GetFilter<T>().Cursor();
        }

        public static bool Has<T>(this EcsWorld world) where T : struct
        {
            return !world.GetFilter<T>().IsEmpty();
        }

        public static EcsCursorDriver<T1, T2> Cursor<T1, T2>(this EcsWorld world)
            where T2 : struct
            where T1 : struct
        {
            return world.GetFilter<T1, T2>().Cursor();
        }
    }
}