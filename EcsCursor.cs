using Leopotam.Ecs;

namespace Kk.CsxLeoEcs
{
    public class EcsCursor<T> : EcsToStringExtensions.IAutoStringProxy
        where T : struct
    {
        internal EcsFilter<T> filter;
        internal int index;

        internal EcsCursor() { }

        public bool Has<TX>() where TX : struct
        {
            return GetEntity().Has<TX>();
        }

        public ref EcsEntity GetEntity()
        {
            return ref filter.GetEntity(index);
        }

        public ref T Get1() => ref filter.Get1(index);

        public ref TX Req<TX>()
            where TX : struct
            => ref filter.GetEntity(index).Req<TX>();

        public object GetAutoStringProxyTarget()
        {
            return GetEntity();
        }
    }

    public class EcsCursor<T1, T2> : EcsToStringExtensions.IAutoStringProxy
        where T1 : struct
        where T2 : struct
    {
        internal EcsFilter<T1, T2> _filter;
        internal int _index;

        internal EcsCursor() { }

        public ref EcsEntity GetEntity() => ref _filter.GetEntity(_index);

        public bool Has<TX>() where TX : struct
        {
            return GetEntity().Has<TX>();
        }
        
        public ref T1 Get1() => ref _filter.Get1(_index);
        public ref T2 Get2() => ref _filter.Get2(_index);

        public ref TX Req<TX>()
            where TX : struct
            => ref _filter.GetEntity(_index).Req<TX>();
        
        public object GetAutoStringProxyTarget()
        {
            return GetEntity();
        }
    }
}