using System;
using System.Linq;
using Leopotam.Ecs;
using UnityEngine;

namespace Kk.CsxLeoEcs
{
    public static class EcsTrace
    {
        public static bool Enabled;

        public static EcsEntity Log(this in EcsEntity entity)
        {
            if (Enabled)
            {
                Type[] components = new Type[0];
                entity.GetComponentTypes(ref components);

                Debug.Log($"New {entity}{string.Join("", components.Select(x => "/" + x.Name))}");
            }

            return entity;
        }

        public static void DestroyLog(this in EcsEntity entity)
        {
            if (Enabled)
            {
                Type[] components = new Type[0];
                entity.GetComponentTypes(ref components);
                Debug.Log($"Destroy {entity}{string.Join("", components.Select(x => "/" + x.Name))}");
            }

            entity.Destroy();
        }
    }
}