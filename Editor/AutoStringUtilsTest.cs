using System;
using System.Collections.Generic;
using Leopotam.Ecs;
using NUnit.Framework;

namespace Kk.CsxLeoEcs.Editor
{
    [TestFixture]
    public class AutoStringUtilsTest
    {
        [Serializable]
        struct A
        {
            public int value;

            public A(int value)
            {
                this.value = value;
            }
        }

        [Test]
        public void EmptyIsOk()
        {
            EcsWorld world = new EcsWorld();
            EcsEntity entity = world.NewEntity();
            string expected = "{\"@type\": \"Leopotam.Ecs.EcsEntity\", \"ID\": \"0-1\", \"alive\": True, \"header\": [], \"data\": {}}";
            string actual = entity.AutoToString();
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void OneSimple()
        {
            EcsWorld world = new EcsWorld();
            EcsEntity entity = world.NewEntity()
                .Replace(new A(12));
            Assert.AreEqual("{\"@type\": \"Leopotam.Ecs.EcsEntity\", \"ID\": \"0-1\", \"alive\": True, \"header\": [\"UnitTests.AutoStringUtilsTest+A\"], \"data\": {\"UnitTests.AutoStringUtilsTest+A\": {\"value\": 12}}}", entity.AutoToString());
        }

        struct B
        {
            public int x;

            public B(int x)
            {
                this.x = x;
            }
        }

        struct C
        {
            public int x;
            public int y;

            public C(int x, int y)
            {
                this.x = x;
                this.y = y;
            }
        }

        [Test]
        public void TwoComplex()
        {
            EcsWorld world = new EcsWorld();
            EcsEntity entity = world.NewEntity()
                .Replace(new B(12))
                .Replace(new C(41, 17));
            Assert.AreEqual("{\"@type\": \"Leopotam.Ecs.EcsEntity\", \"ID\": \"0-1\", \"alive\": True, \"header\": [\"UnitTests.AutoStringUtilsTest+B\", \"UnitTests.AutoStringUtilsTest+C\"], \"data\": {\"UnitTests.AutoStringUtilsTest+B\": {\"x\": 12}, \"UnitTests.AutoStringUtilsTest+C\": {\"x\": 41, \"y\": 17}}}", entity.AutoToString());
        }

        struct D
        {
            public B b;

            public D(B b)
            {
                this.b = b;
            }
        }

        [Test]
        public void NestedComplex()
        {
            EcsWorld world = new EcsWorld();
            EcsEntity entity = world.NewEntity()
                .Replace(new D(new B(12)));
            Assert.AreEqual("{\"@type\": \"Leopotam.Ecs.EcsEntity\", \"ID\": \"0-1\", \"alive\": True, \"header\": [\"UnitTests.AutoStringUtilsTest+D\"], \"data\": {\"UnitTests.AutoStringUtilsTest+D\": {\"b\": {\"x\": 12}}}}", entity.AutoToString());
        }

        struct E
        {
            public B[] bsArray;
            public List<B> bsList;

            public E(B[] bsArray, List<B> bsList)
            {
                this.bsArray = bsArray;
                this.bsList = bsList;
            }
        }

        [Test]
        public void NestedListComplex()
        {
            EcsWorld world = new EcsWorld();
            EcsEntity entity = world.NewEntity()
                .Replace(new E(new []{new B(1), new B(2)}, new List<B> {new B(3), new B(4)}));
            Assert.AreEqual("{\"@type\": \"Leopotam.Ecs.EcsEntity\", \"ID\": \"0-1\", \"alive\": True, \"header\": [\"UnitTests.AutoStringUtilsTest+E\"], \"data\": {\"UnitTests.AutoStringUtilsTest+E\": {\"bsArray\": {\"@type\": \"UnitTests.AutoStringUtilsTest+B[]\", \"items\": [{\"x\": 1}, {\"x\": 2}]}, \"bsList\": {\"@type\": \"System.Collections.Generic.List`1[[UnitTests.AutoStringUtilsTest+B, Kk.CsxLeoEcs.Editor, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null]]\", \"items\": [{\"x\": 3}, {\"x\": 4}]}}}}", entity.AutoToString());
        }

        class LoopA
        {
            public LoopB b;
        }

        class LoopB
        {
            public LoopA a;
        }

        struct LoopComp
        {
            public LoopA a;

            public LoopComp(LoopA a)
            {
                this.a = a;
            }
        }

        [Test]
        public void ReferenceLoop()
        {
            LoopA a = new LoopA();
            LoopB b = new LoopB();
            a.b = b;
            b.a = a;
            EcsWorld world = new EcsWorld();
            EcsEntity entity = world.NewEntity()
                .Replace(new LoopComp(a));

            string actual = entity.AutoToString();
            Assert.AreEqual("{\"@type\": \"Leopotam.Ecs.EcsEntity\", \"ID\": \"0-1\", \"alive\": True, \"header\": [\"UnitTests.AutoStringUtilsTest+LoopComp\"], \"data\": {\"UnitTests.AutoStringUtilsTest+LoopComp\": {\"a\": {\"@type\": \"UnitTests.AutoStringUtilsTest+LoopA\", \"fields\": {\"b\": {\"@type\": \"UnitTests.AutoStringUtilsTest+LoopB\", \"fields\": {\"a\": {\"@type\": \"UnitTests.AutoStringUtilsTest+LoopA\", \"@error\": \"(LOOP)\"}}}}}}}}", actual);
        }
    }
}