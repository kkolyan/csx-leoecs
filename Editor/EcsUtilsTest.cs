using System.Linq;
using Leopotam.Ecs;
using NUnit.Framework;

namespace Kk.CsxLeoEcs.Editor
{
    [TestFixture]
    public class EcsUtilsTest
    {
        [Test]
        public void SelectFromCursor()
        {
            EcsWorld world = new EcsWorld();
            world.NewEntity()
                .Replace(1);
            world.NewEntity()
                .Replace(2);
            world.NewEntity()
                .Replace(3);

            int[] array = world.Cursor<int>().Select(x => x.Get1()).ToArray();
            
            Assert.AreEqual("1,2,3", string.Join(",", array));
        }
        
        [Test]
        public void SingleFromCursorNotMessedByGarbage()
        {
            EcsWorld world = new EcsWorld();
            
            EcsEntity e1 = world.NewEntity()
                .Replace(1);
            EcsEntity e2 = world.NewEntity()
                .Replace(2);
            EcsEntity e3 = world.NewEntity()
                .Replace(3);
            
            e1.Destroy();
            e2.Destroy();
            e3.Destroy();

            world.NewEntity()
                .Replace(7);

            int single = world.Single<int>();
            Assert.AreEqual(7, single);
        }
        
        [Test]
        public void SingleOrNullFromCursorNotMessedByGarbageWhenNotNull()
        {
            EcsWorld world = new EcsWorld();
            
            EcsEntity e1 = world.NewEntity()
                .Replace(1);
            EcsEntity e2 = world.NewEntity()
                .Replace(2);
            EcsEntity e3 = world.NewEntity()
                .Replace(3);
            
            e1.Destroy();
            e2.Destroy();
            e3.Destroy();

            world.NewEntity()
                .Replace(7);

            int? single = world.SingleOrNull<int>();
            Assert.AreEqual(7, single);
        }
        
        [Test]
        public void SingleOrNullFromCursorNotMessedByGarbageWhenNull()
        {
            EcsWorld world = new EcsWorld();
            
            EcsEntity e1 = world.NewEntity()
                .Replace(1);
            EcsEntity e2 = world.NewEntity()
                .Replace(2);
            EcsEntity e3 = world.NewEntity()
                .Replace(3);
            
            e1.Destroy();
            e2.Destroy();
            e3.Destroy();

            int? single = world.SingleOrNull<int>();
            Assert.AreEqual(null, single);
        }
    }
}