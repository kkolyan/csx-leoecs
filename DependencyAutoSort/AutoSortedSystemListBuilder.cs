using System;
using System.Collections.Generic;
using System.Linq;
using Kk.CsxCore;
using Kk.CsxCore.Graph;
using Leopotam.Ecs;

namespace Kk.CsxLeoEcs.DependencyAutoSort
{
    public class AutoSortedSystemListBuilder<TBarrier>
    {
        private ISet<SystemGraphNode<TBarrier>> _nodes = new HashSet<SystemGraphNode<TBarrier>>(
            SystemGraphNode<TBarrier>.SystemBarrierComparer
        );

        private ISet<Edge<SystemGraphNode<TBarrier>>> _edges = new HashSet<Edge<SystemGraphNode<TBarrier>>>(
            Edge<SystemGraphNode<TBarrier>>.Comparer
        );

        private IDictionary<Type, IList<SystemGraphNode<TBarrier>>> _systemNodeByType
            = new Dictionary<Type, IList<SystemGraphNode<TBarrier>>>();

        private IList<TBarrier> _cumulativeBarriers = new List<TBarrier>();

        public DependencyBuilder<TBarrier> Add<T>() where T : IEcsSystem, new()
        {
            var node = new SystemGraphNode<TBarrier>(new T());
            _nodes.Add(node);
            var theSameOfType = _systemNodeByType.GetOrDefault(typeof(T));
            if (theSameOfType == null)
            {
                theSameOfType = new List<SystemGraphNode<TBarrier>>();
                _systemNodeByType[typeof(T)] = theSameOfType;
            }

            theSameOfType.Add(node);

            foreach (var cumulativeBarrier in _cumulativeBarriers)
            {
                AfterBarrier(node, cumulativeBarrier);
            }

            return new DependencyBuilder<TBarrier>(node, this);
        }

        /// <summary>
        /// the only case for this method - is initialization of "global entity" which is used in almost all entities
        /// </summary>
        /// <param name="barrier"></param>
        public void AllBelowAfter(TBarrier barrier)
        {
            _cumulativeBarriers.Add(barrier);
        }

        public IList<string> GetOrderReport()
        {
            var orderLock = new List<string>();
            orderLock.Add("# this list is generated to track changes in auto-defined systems order");
            var sortedNodes = BuildSortedNodesList();
            foreach (var node in sortedNodes)
            {
                orderLock.Add(node.System?.GetType()?.FullName ?? node._barrier.ToString());
            }

            return orderLock;
        }

        public IList<IEcsSystem> BuildSortedSystemList()
        {
            var sortedNodes = BuildSortedNodesList();
            return sortedNodes
                .Select(x => x.System)
                .Where(x => x != null)
                .ToList();
        }

        private IList<SystemGraphNode<TBarrier>> BuildSortedNodesList()
        {
            var roots = _nodes.ToList();
            foreach (var edge in _edges)
            {
                roots.Remove(edge.To);
            }

            var sortedNodes = new KahnTopologicalSorter<SystemGraphNode<TBarrier>>()
                .GetSorted(roots, _edges);
            return sortedNodes;
        }

        internal void AfterBarrier(SystemGraphNode<TBarrier> node, TBarrier barrier)
        {
            _nodes.Add(new SystemGraphNode<TBarrier>(barrier));
            _edges.Add(new Edge<SystemGraphNode<TBarrier>>(new SystemGraphNode<TBarrier>(barrier), node));
        }

        internal void BeforeBarrier(SystemGraphNode<TBarrier> node, TBarrier barrier)
        {
            _nodes.Add(new SystemGraphNode<TBarrier>(barrier));
            _edges.Add(new Edge<SystemGraphNode<TBarrier>>(node, new SystemGraphNode<TBarrier>(barrier)));
        }

        internal void AfterSystem<T>(SystemGraphNode<TBarrier> node)
            where T : IEcsSystem
        {
            var otherNode = _systemNodeByType.GetOrDefault(typeof(T)).Single();
            _edges.Add(new Edge<SystemGraphNode<TBarrier>>(otherNode, node));
        }
    }
}