using Leopotam.Ecs;

namespace Kk.CsxLeoEcs.DependencyAutoSort
{
    public readonly struct DependencyBuilder<TBarrier>
    {
        private readonly SystemGraphNode<TBarrier> _node;
        private readonly AutoSortedSystemListBuilder<TBarrier> _parent;

        internal DependencyBuilder(SystemGraphNode<TBarrier> node, AutoSortedSystemListBuilder<TBarrier> parent)
        {
            _node = node;
            _parent = parent;
        }

        public DependencyBuilder<TBarrier> After(TBarrier barrier)
        {
            _parent.AfterBarrier(_node, barrier);
            return this;
        }

        public DependencyBuilder<TBarrier> Before(TBarrier barrier)
        {
            _parent.BeforeBarrier(_node, barrier);
            return this;
        }

        public DependencyBuilder<TBarrier> After<T>()
            where T: IEcsSystem
        {
            _parent.AfterSystem<T>(_node);
            return this;
        }
    }
}