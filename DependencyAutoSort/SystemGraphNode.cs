using System.Collections.Generic;
using Leopotam.Ecs;

namespace Kk.CsxLeoEcs.DependencyAutoSort
{
    internal readonly struct SystemGraphNode<T>
    {
        public readonly IEcsSystem System;
        public readonly T _barrier;

        public SystemGraphNode(IEcsSystem system) : this()
        {
            System = system;
        }

        public SystemGraphNode(T barrier) : this()
        {
            _barrier = barrier;
        }

        private sealed class SystemBarrierEqualityComparer : IEqualityComparer<SystemGraphNode<T>>
        {
            public bool Equals(SystemGraphNode<T> x, SystemGraphNode<T> y)
            {
                return Equals(x.System, y.System) && EqualityComparer<T>.Default.Equals(x._barrier, y._barrier);
            }

            public int GetHashCode(SystemGraphNode<T> obj)
            {
                unchecked
                {
                    return ((obj.System != null ? obj.System.GetHashCode() : 0) * 397) ^ EqualityComparer<T>.Default.GetHashCode(obj._barrier);
                }
            }
        }

        public static IEqualityComparer<SystemGraphNode<T>> SystemBarrierComparer { get; } = new SystemBarrierEqualityComparer();

        public override string ToString()
        {
            return $"{nameof(System)}: {System}, {nameof(_barrier)}: {_barrier}";
        }
    }
}