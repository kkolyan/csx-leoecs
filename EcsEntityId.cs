using System;
using System.Runtime.CompilerServices;
using Leopotam.Ecs;

namespace Kk.CsxLeoEcs
{
    [Serializable]
    public struct EcsEntityId
    {
        public static readonly EcsEntityId Null = new EcsEntityId();
        
        public int entityId;
        public int entityGen;

        public EcsEntityId(int entityId, int entityGen)
        {
            this.entityId = entityId;
            this.entityGen = entityGen;
        }

#if DEBUG
        public override string ToString()
        {
            return this.IsNull () ? "EntityId-Null" : $"EntityId-{entityId}:{entityGen}";
        }
#endif

        public bool Equals(EcsEntityId other)
        {
            return entityId == other.entityId && entityGen == other.entityGen;
        }

        [MethodImpl (MethodImplOptions.AggressiveInlining)]
        public override bool Equals(object obj)
        {
            return obj is EcsEntityId other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (entityId * 397) ^ entityGen;
            }
        }

        [MethodImpl (MethodImplOptions.AggressiveInlining)]
        public static bool operator ==(EcsEntityId a, EcsEntityId b)
        {
            return a.entityId == b.entityId && a.entityGen == b.entityGen;
        }

        [MethodImpl (MethodImplOptions.AggressiveInlining)]
        public static bool operator !=(EcsEntityId a, EcsEntityId b)
        {
            return a.entityId != b.entityId || a.entityGen != b.entityGen;
        }
    }
    
    public static class EntityIdMethods
    {
        [MethodImpl (MethodImplOptions.AggressiveInlining)]
        public static bool IsNull (this in EcsEntityId value) {
            return value.entityId == 0 && value.entityGen == 0;
        }

        [MethodImpl (MethodImplOptions.AggressiveInlining)]
        public static EcsEntity Resolve(this in EcsEntityId value, EcsWorld world)
        {
            return world.RestoreEntityFromInternalId(value.entityId, value.entityGen);
        }
        
    }
}