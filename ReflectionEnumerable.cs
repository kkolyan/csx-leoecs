using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace Kk.CsxLeoEcs
{
    internal class ReflectionEnumerable : IEnumerable<object>
    {
        private readonly object _target;
        
        public ReflectionEnumerable(object target)
        {
            _target = target;
        }

        public Enumerator GetEnumerator()
        {
            return new Enumerator(_target.GetType().GetMethod("GetEnumerator").Invoke(_target, new object[0]));
        }

        IEnumerator<object> IEnumerable<object>.GetEnumerator()
        {
            return GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public class Enumerator: IEnumerator<object>
        {
            private readonly object _target;
            
            public Enumerator(object target)
            {
                _target = target;
            }

            public bool MoveNext()
            {
                if (_target == null)
                {
                    return false;
                }

                MethodInfo method = _target.GetType().GetMethod("MoveNext");
                if (method == null)
                {
                    throw new Exception($"enumerator doesn't have MoveNext method. type: {_target.GetType()}");
                }
                return (bool) method.Invoke(_target, new object[0]);
            }

            public void Reset()
            {
                _target.GetType().GetMethod("Reset", BindingFlags.Instance | BindingFlags.Public)?.Invoke(_target, new object[0]);
            }

            public object Current => _target.GetType().GetProperty("Current").GetValue(_target);

            object IEnumerator.Current => Current;

            public void Dispose()
            {
                _target.GetType().GetMethod("Dispose")?.Invoke(_target, new object[0]);
            }
        }
    }
}